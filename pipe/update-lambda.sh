#!/bin/bash

aws_extra_args=""
if [[ "${DEBUG}" == true ]]; then
    aws_extra_args="--debug"
fi

## Upload and publish the lambda fuction atomically.
update_lambda() {
  info "Updating Lambda function."
  run aws lambda update-function-code  --function-name "${FUNCTION_NAME}" --publish --zip-file "fileb://${ZIP_FILE}" ${aws_extra_args}
  if [[ "${status}" -ne "0" ]]; then
    fail "Failed to update Lambda function code."
  fi
  info "Update command succeeded."
}

# Write results to file
write_result() {
  filename=$BITBUCKET_PIPE_SHARED_STORAGE_DIR/aws-lambda-deploy-env
  info "Writing results to file $filename."
  cat "${output_file}" > $filename
  cat $filename
}

update() {
  update_lambda
  write_result
  success "Successfully updated function."
}
